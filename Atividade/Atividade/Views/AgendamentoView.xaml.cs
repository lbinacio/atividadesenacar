﻿using Atividade.Data;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Atividade.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgendamentoView : ContentPage
	{
        public AgendamentoView (Tabela tabela)
		{
			InitializeComponent();
            this.Title = tabela.Nome;
		}

        private void ButtonAgendar_Clicked(object sender, EventArgs e)
        {
            using (var conexao = DependencyService.Get<ISQLite>().conexao())
            {

            }
                DisplayAlert("Sucesso", "Agendamento realizado com sucesso!Obrigado!", "Ok");
        }
        public async void Permissao()
        {
            try
            {
                var status = await
                CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await

                    CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        await DisplayAlert("Permissão", "Precisamos da sua permissão para armazenar dados no dispositivo.", "OK");
                    
}
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[]

                    { Permission.Storage });

                    status = results[Permission.Storage];
                }
                if (status == PermissionStatus.Granted)
                {
                    //await DisplayAlert("Sucesso!", "Prossiga com a operação", "OK");
                    FazerAgendamento();
                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Atenção", "Para utilizar os serviços de persistência de dados, aceite a permissão solicitada pelo dispositivo.", "OK");
                }
            }
            catch (Exception ex)
            {
                //await DisplayAlert("Erro", "Erro no processo " + ex.Message, "OK");
            }
        }

        private void FazerAgendamento()
        {
            Console.WriteLine("Iniciando Agendamento");
            using (var conexao = DependencyService.Get<ISQLite>().conexao()) 
            {
                AgendamentoDAO dao = new AgendamentoDAO(conexao);
                dao.Salvar(new Models.Agendamento("Compra Agendada", "3456", "lbinacio@hotmail.com"));

                DisplayAlert("Sucesso", "Agendamento Realizado", "Ok");
            }

        }


    }
}