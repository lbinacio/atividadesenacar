﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Atividade.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{
        private const float VIDROS_ELETRICOS = 545;
        private const float TRAVAS_ELETRICAS = 260;
        private const float AR_CONDICIONADO = 480;
        private const float CAMERA_DE_RE = 180;
        private const float CAMBIO = 460;
        private const float SUSPENCAO = 380;
        private const float FREIOS = 245;

        public string TextoVidros_Eletricos
        {
            get
            {
                return string.Format("Vidros Elétricos - R$ {545}", VIDROS_ELETRICOS);
            }
        }

        public string TextoTravas_Eletricas
        {
            get
            {
                return string.Format("Travas Elétricas - R$ {260}", TRAVAS_ELETRICAS);
            }
        }

        public string TextoAr_Condicionado
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {480}", AR_CONDICIONADO);
            }
        }

        public string TextoCamera_De_Re
        {
            get
            {
                return string.Format("Câmera de Ré - R$ {180}", CAMERA_DE_RE);
            }
        }

        public string Cambio
        {
            get
            {
                return string.Format("Câmbio - R$ {460}", CAMBIO);
            }
        }

        public string Suspencao
        {
            get
            {
                return string.Format("Suspenção - R$ {380}", SUSPENCAO);
            }
        }

        public string Freios 
        {
            get
            {
                return string.Format("Freios - R$ {245}", FREIOS);
            }
        }

        public Tabela Tabela { get; set; }
		public DescricaoView (Tabela tabela)
		{
			InitializeComponent ();

            this.Title = tabela.Nome;
            this.Tabela = tabela;
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(this.Tabela));
        }

        bool incluiVidros_Eletricos;
        public bool IncluiVidros_Eletricos
        {
            get
            {
                return incluiVidros_Eletricos;
            }
            set
            {
                incluiVidros_Eletricos = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiVidros_Eletricos)
                //    DisplayAlert("Vidros Elétricos", "Ativo", "Ok");
                //else
                //    DisplayAlert("Vidros Elétricos", "Inativo", "Ok");

            }
        }

        bool incluiTravas_Eletricas;
        public bool IncluiTravas_Eletricas
        {
            get
            {
                return incluiTravas_Eletricas;
            }
            set
            {
                incluiTravas_Eletricas = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTravas_Eletricas)
                //    DisplayAlert("Travas Elétricas", "Ativo", "Ok");
                //else
                //    DisplayAlert("Travas Elétricas", "Inativo", "Ok");

            }
        }

        bool incluiAr_Condicionado;
        public bool IncluiAr_Condicionado
        {
            get
            {
                return incluiAr_Condicionado;
            }
            set
            {
                incluiAr_Condicionado = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiAr_Condicionado)
                //    DisplayAlert("Ar Condicionado", "Ativo", "Ok");
                //else
                //    DisplayAlert("Ar Condicionado", "Inativo", "Ok");

            }
        }


        bool incluiCamera_De_Re;
        public bool IncluiCamera_De_Re
        {
            get
            {
                return incluiCamera_De_Re;
            }
            set
            {
                incluiCamera_De_Re = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiCamera_De_Re)
                //    DisplayAlert("Câmera De Ré", "Ativo", "Ok");
                //else
                //    DisplayAlert("Câmera De Ré", "Inativo", "Ok");

            }
        }

        bool incluiCambio;
        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiCambio)
                //    DisplayAlert("Câmbio", "Ativo", "Ok");
                //else
                //    DisplayAlert("Câmbio", "Inativo", "Ok");

            }
        }

        bool incluiSuspencao;
        public bool IncluiSuspencao
        {
            get
            {
                return incluiSuspencao;
            }
            set
            {
                incluiSuspencao = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiSuspencao)
                //    DisplayAlert("Suspenção", "Ativo", "Ok");
                //else
                //    DisplayAlert("Suspenção", "Inativo", "Ok");

            }
        }


        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiFreios)
                //    DisplayAlert("Freios", "Ativo", "Ok");
                //else
                //    DisplayAlert("Freios", "Inativo", "Ok");

            }
        }

        public string ValorTotal
        {
            get
            {
                return string.Format("Valor Total: R$ {0}",
                    Tabela.Preco
                    + (IncluiVidros_Eletricos ? VIDROS_ELETRICOS: 0)
                    + (IncluiTravas_Eletricas ? TRAVAS_ELETRICAS : 0)
                    + (IncluiAr_Condicionado ? AR_CONDICIONADO : 0)
                    + (IncluiCamera_De_Re ? CAMERA_DE_RE : 0)
                    + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspencao ? SUSPENCAO : 0)
                    + (IncluiFreios ? FREIOS : 0));

            }
        }
    }
}