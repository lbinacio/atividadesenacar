﻿using Atividade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Atividade.Views
{
    public class Tabela
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public float Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }

        public string Nome { get; internal set; }
    }


    public partial class MainPage : ContentPage
    {
        public List<Tabela> Tabela { get; set; }

        public MainPage()
        {
            InitializeComponent();

            this.Tabela = new List<Tabela>
        {
            new Tabela {Marca = "Chevrolet", Modelo = "Onix", Preco = 44000},
            new Tabela {Marca = "Hyundai", Modelo = "Hb20", Preco = 42000},
            new Tabela {Marca = "Renault", Modelo = "Sandero", Preco = 39000},
            new Tabela {Marca = "Ford", Modelo = "Fiesta", Preco = 29000},
            new Tabela {Marca = "Honda", Modelo= "Civic", Preco = 84000}
        };

            this.BindingContext = this;
        }


        private void ListViewTabela_ItemTapped(object sender,
            ItemTappedEventArgs e)
        {
            var tabela = (Tabela)e.Item;
            Navigation.PushAsync(new DescricaoView(tabela));
            //DisplayAlert("Serviço",
            //    string.Format("Você selecionou o serviço '{0}'. Valor: {1}",
            //    servico.Nome, servico.PrecoFormatado), "Ok");
        }

    }
}
