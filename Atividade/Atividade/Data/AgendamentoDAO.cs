﻿using Atividade.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atividade.Data
{

    class AgendamentoDAO
    {
        readonly SQLiteConnection conexao;

        public AgendamentoDAO(SQLiteConnection conexao)
        {
            this.conexao = conexao;
            this.conexao.CreateTable<Agendamento>();
        }

        internal void Salvar(Agendamento agendamento)
        {
            conexao.Insert(agendamento);
        }
    } 
}
