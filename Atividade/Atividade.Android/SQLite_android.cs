﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Atividade.Data;
using Atividade.Droid;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(SQLite_android))]

namespace Atividade.Droid
{
    class SQLite_android : ISQLite
    {
        private const string arquivoDb = "Atividade.db3";

        public SQLiteConnection conexao()
        {
            var pathDb = Path.Combine(Android.OS.Environment
                .ExternalStorageDirectory.Path, arquivoDb);

            return new SQLite.SQLiteConnection(pathDb);


            //return new SQLite.SQLiteConnection("Atividade.db3");
            ////throw new NotImplementedException();
        }
    }
}